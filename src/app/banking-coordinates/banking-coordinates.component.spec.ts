import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BankingCoordinatesComponent } from './banking-coordinates.component';

describe('BankingCoordinatesComponent', () => {
  let component: BankingCoordinatesComponent;
  let fixture: ComponentFixture<BankingCoordinatesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BankingCoordinatesComponent]
    });
    fixture = TestBed.createComponent(BankingCoordinatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
