import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralConditionOfSalesComponent } from './general-condition-of-sales.component';

describe('GeneralConditionOfSalesComponent', () => {
  let component: GeneralConditionOfSalesComponent;
  let fixture: ComponentFixture<GeneralConditionOfSalesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [GeneralConditionOfSalesComponent]
    });
    fixture = TestBed.createComponent(GeneralConditionOfSalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
