import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ParameterComponent } from './parameter/parameter.component';
import { MyAccountComponent } from './my-account/my-account.component';
import { NotificationComponent } from './notification/notification.component';
import { GeneralConditionOfSalesComponent } from './general-condition-of-sales/general-condition-of-sales.component';
import { PersonalInfoComponent } from './personal-info/personal-info.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ProfileComponent } from './profile/profile.component';
import { ConnexionSecuriteComponent } from './connexion-securite/connexion-securite.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { VendorTypeComponent } from './vendor-type/vendor-type.component';
import { BoutiqueInfoComponent } from './boutique-info/boutique-info.component';
import { BankingCoordinatesComponent } from './banking-coordinates/banking-coordinates.component';
import { CentralSellerComponent } from './central-seller/central-seller.component';
import { OrdersComponent } from './orders/orders.component';
import { ManageScheduleComponent } from './manage-schedule/manage-schedule.component';
import { ProductComponent } from './product/product.component';
import { ProductDComponent } from './product-d/product-d.component';
import { AddProductComponent } from './add-product/add-product.component';
import { FinanceComponent } from './finance/finance.component';
import { WithdrawalRequestComponent } from './withdrawal-request/withdrawal-request.component';
import { TransactionDetailComponent } from './transaction-detail/transaction-detail.component';
import { MessageComponent } from './message/message.component';
import { BoutiqueComponent } from './boutique/boutique.component';
import { BoutiqueRatingComponent } from './boutique-rating/boutique-rating.component';
import { ShopProfileComponent } from './shop-profile/shop-profile.component';
import { CouponReductionComponent } from './coupon-reduction/coupon-reduction.component';
import { NewVoucheComponent } from './new-vouche/new-vouche.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {
    path:'parameter',
    component:ParameterComponent,
    pathMatch:"full"
  },
  {
    path:'my_account',
    component:MyAccountComponent,
    pathMatch:"full"
  },
  {
    path:'notification',
    component:NotificationComponent,
    pathMatch:"full"
  },
  {
    path:'general_condition',
    component:GeneralConditionOfSalesComponent,
    pathMatch:"full"
  },
  {
    path:'privacy_policy',
    component:PrivacyPolicyComponent,
    pathMatch:"full"
  },
  {
    path:'personal_info',
    component:PersonalInfoComponent,
    pathMatch:"full"
  },
  {
    path:'contact_us',
    component:ContactUsComponent,
    pathMatch:"full"
  },
  {
    path:'about_us',
    component:AboutUsComponent,
    pathMatch:"full"
  },
  {
    path:'profile',
    component:ProfileComponent,
    pathMatch:"full"
  },
  {
    path:'connexion_securite',
    component:ConnexionSecuriteComponent,
    pathMatch:"full"
  },
  
  {
    path:'register',
    component:RegisterComponent,
    pathMatch:"full"
  },
  {
    path:'login',
    component:LoginComponent,
    pathMatch:"full"
  },
  {
    path:'vendor_type',
    component:VendorTypeComponent,
    pathMatch:"full"
  },
  {
    path:'boutique_info',
    component:BoutiqueInfoComponent,
    pathMatch:"full"
  },
  {
    path:'banking_coordinates',
    component:BankingCoordinatesComponent,
    pathMatch:"full"
  },
  {
    path:'centra_seller',
    component:CentralSellerComponent,
    pathMatch:"full"
  },
  {
    path:'',
    component:HomeComponent,
    pathMatch:"full"
  },
  {
    path:'order',
    component:OrdersComponent,
    pathMatch:"full"
  },
  {
    path:'manage_schedule',
    component:ManageScheduleComponent,
    pathMatch:"full"
  },
  {
    path:'product',
    component:ProductComponent,
    pathMatch:"full"
  },
  {
    path:'product_d',
    component:ProductDComponent,
    pathMatch:"full"
  },
  {
    path:'add_product',
    component:AddProductComponent,
    pathMatch:"full"
  },
  {
    path:'finance',
    component:FinanceComponent,
    pathMatch:"full"
  },
  {
    path:'withdrawal_request',
    component:WithdrawalRequestComponent,
    pathMatch:"full"
  },
  {
    path:'transaction_detail',
    component:TransactionDetailComponent,
    pathMatch:"full"
  },
  {
    path:'message',
    component:MessageComponent,
    pathMatch:"full"
  },
  {
    path:'boutique',
    component:BoutiqueComponent,
    pathMatch:"full"
  },
  {
    path:'boutique_rating',
    component:BoutiqueRatingComponent,
    pathMatch:"full"
  },
  {
    path:'shop_profile',
    component:ShopProfileComponent,
    pathMatch:"full"
  },
  {
    path:'coupon_reduction',
    component:CouponReductionComponent,
    pathMatch:"full"
  },
  {
    path:'new_voucher',
    component:NewVoucheComponent,
    pathMatch:"full"
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
