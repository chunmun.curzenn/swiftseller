import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ParameterComponent } from './parameter/parameter.component';
import { HeaderComponent } from './header/header.component';
import { MyAccountComponent } from './my-account/my-account.component';
import { NotificationComponent } from './notification/notification.component';
import { GeneralConditionOfSalesComponent } from './general-condition-of-sales/general-condition-of-sales.component';
import { PersonalInfoComponent } from './personal-info/personal-info.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ProfileComponent } from './profile/profile.component';
import { ConnexionSecuriteComponent } from './connexion-securite/connexion-securite.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { VendorTypeComponent } from './vendor-type/vendor-type.component';
import { BoutiqueInfoComponent } from './boutique-info/boutique-info.component';
import { BankingCoordinatesComponent } from './banking-coordinates/banking-coordinates.component';
import { CentralSellerComponent } from './central-seller/central-seller.component';
import { FooterComponent } from './footer/footer.component';
import { OrdersComponent } from './orders/orders.component';
import { ManageScheduleComponent } from './manage-schedule/manage-schedule.component';
import { ProductComponent } from './product/product.component';
import { ProductDComponent } from './product-d/product-d.component';
import { AddProductComponent } from './add-product/add-product.component';
import { FinanceComponent } from './finance/finance.component';
import { WithdrawalRequestComponent } from './withdrawal-request/withdrawal-request.component';
import { TransactionDetailComponent } from './transaction-detail/transaction-detail.component';
import { MessageComponent } from './message/message.component';
import { BoutiqueComponent } from './boutique/boutique.component';
import { BoutiqueRatingComponent } from './boutique-rating/boutique-rating.component';
import { ShopProfileComponent } from './shop-profile/shop-profile.component';
import { CouponReductionComponent } from './coupon-reduction/coupon-reduction.component';
import { NewVoucheComponent } from './new-vouche/new-vouche.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    ParameterComponent,
    HeaderComponent,
    MyAccountComponent,
    NotificationComponent,
    GeneralConditionOfSalesComponent,
    PersonalInfoComponent,
    ContactUsComponent,
    AboutUsComponent,
    ProfileComponent,
    ConnexionSecuriteComponent,
    LoginComponent,
    RegisterComponent,
    PrivacyPolicyComponent,
    VendorTypeComponent,
    BoutiqueInfoComponent,
    BankingCoordinatesComponent,
    CentralSellerComponent,
    FooterComponent,
    OrdersComponent,
    ManageScheduleComponent,
    ProductComponent,
    ProductDComponent,
    AddProductComponent,
    FinanceComponent,
    WithdrawalRequestComponent,
    TransactionDetailComponent,
    MessageComponent,
    BoutiqueComponent,
    BoutiqueRatingComponent,
    ShopProfileComponent,
    CouponReductionComponent,
    NewVoucheComponent,
    SidebarComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
