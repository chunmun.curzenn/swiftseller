import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoutiqueRatingComponent } from './boutique-rating.component';

describe('BoutiqueRatingComponent', () => {
  let component: BoutiqueRatingComponent;
  let fixture: ComponentFixture<BoutiqueRatingComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BoutiqueRatingComponent]
    });
    fixture = TestBed.createComponent(BoutiqueRatingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
