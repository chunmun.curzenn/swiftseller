import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewVoucheComponent } from './new-vouche.component';

describe('NewVoucheComponent', () => {
  let component: NewVoucheComponent;
  let fixture: ComponentFixture<NewVoucheComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [NewVoucheComponent]
    });
    fixture = TestBed.createComponent(NewVoucheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
