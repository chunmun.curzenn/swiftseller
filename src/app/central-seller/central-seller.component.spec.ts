import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CentralSellerComponent } from './central-seller.component';

describe('CentralSellerComponent', () => {
  let component: CentralSellerComponent;
  let fixture: ComponentFixture<CentralSellerComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CentralSellerComponent]
    });
    fixture = TestBed.createComponent(CentralSellerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
