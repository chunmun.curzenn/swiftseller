import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit{
  myTogglehide:any

  ngOnInit(): void {
    this.myTogglehide = true
  }
  toggleNavbar(){
    this.myTogglehide = !this.myTogglehide;
  }
}
