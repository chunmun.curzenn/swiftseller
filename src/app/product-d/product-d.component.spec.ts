import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductDComponent } from './product-d.component';

describe('ProductDComponent', () => {
  let component: ProductDComponent;
  let fixture: ComponentFixture<ProductDComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ProductDComponent]
    });
    fixture = TestBed.createComponent(ProductDComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
