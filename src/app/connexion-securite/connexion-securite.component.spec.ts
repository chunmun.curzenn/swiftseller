import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnexionSecuriteComponent } from './connexion-securite.component';

describe('ConnexionSecuriteComponent', () => {
  let component: ConnexionSecuriteComponent;
  let fixture: ComponentFixture<ConnexionSecuriteComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ConnexionSecuriteComponent]
    });
    fixture = TestBed.createComponent(ConnexionSecuriteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
